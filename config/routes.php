<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope(
    '/',
    function (RouteBuilder $routes) {
        // Register scoped middleware for in scopes.
        $routes->registerMiddleware(
            'csrf',
            new CsrfProtectionMiddleware(
                [
                    'httpOnly' => true
                ]
            )
        );
        $routes->applyMiddleware('csrf');

        Router::scope(
            '/users',
            function (RouteBuilder $routes) {
                $routes->setExtensions(['json']);
                Router::connect(
                    '/users/login',
                    ['controller' => 'Users', 'action' => 'login']
                );
                Router::connect(
                    '/users/logout',
                    ['controller' => 'Users', 'action' => 'logout']
                );
                Router::connect(
                    '/users/register',
                    ['controller' => 'Users', 'action' => 'register']
                );
                Router::connect(
                    '/users/edit',
                    ['controller' => 'Users', 'action' => 'editUser']
                );
                Router::connect(
                    '/users/change-password',
                    ['controller' => 'Users', 'action' => 'changePassword']
                );
                Router::connect(
                    '/users-information/view/:user_id',
                    ['controller' => 'Users', 'action' => 'viewUserInfo'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/users/view/',
                    ['controller' => 'Users', 'action' => 'viewUser']
                );
                Router::connect(
                    '/users/activate/:email/:activateCode',
                    ['controller' => 'Users', 'action' => 'activateUserAccount'],
                    ['pass' => ['email', 'activateCode']]
                );
                $routes->fallbacks('InflectedRoute');
            }
        );

        Router::scope(
            '/posts',
            function (RouteBuilder $routes) {
                $routes->setExtensions(['json']);
                Router::connect(
                    '/posts/add/',
                    ['controller' => 'Posts', 'action' => 'addPost']
                );
                Router::connect(
                    '/posts/edit/:post_id',
                    ['controller' => 'Posts', 'action' => 'editPost'],
                    ['pass' => ['post_id']]
                );
                Router::connect(
                    '/posts/remove/',
                    ['controller' => 'Posts', 'action' => 'removePost']
                );
                Router::connect(
                    '/posts/view/',
                    ['controller' => 'Posts', 'action' => 'viewPost']
                );
                Router::connect(
                    '/users-post/view/:user_id',
                    ['controller' => 'Posts', 'action' => 'viewUserPost'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/posts-information/view/:post_id',
                    ['controller' => 'Posts', 'action' => 'viewPostInfo'],
                    ['pass' => ['post_id']]
                );
                Router::connect(
                    '/posts/share/',
                    ['controller' => 'Posts', 'action' => 'sharePost']
                );
                $routes->fallbacks('InflectedRoute');
            }
        );

        Router::scope(
            '/followers',
            function (RouteBuilder $routes) {
                Router::connect(
                    '/users/follow',
                    ['controller' => 'Followers', 'action' => 'followUser']
                );
                Router::connect(
                    '/users/unfollow',
                    ['controller' => 'Followers', 'action' => 'unfollowUser']
                );
                Router::connect(
                    '/users-following/:user_id',
                    ['controller' => 'Followers', 'action' => 'userFollowing'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/users-follower/:user_id',
                    ['controller' => 'Followers', 'action' => 'userFollower'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/users/check-following/:user_id/',
                    ['controller' => 'Followers', 'action' => 'checkFollowing'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/users/check-follower/:user_id/',
                    ['controller' => 'Followers', 'action' => 'checkFollower'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/users/total-follower/:user_id',
                    ['controller' => 'Followers', 'action' => 'totalFollower'],
                    ['pass' => ['user_id']]
                );
                Router::connect(
                    '/users/total-following/:user_id',
                    ['controller' => 'Followers', 'action' => 'totalFollowing'],
                    ['pass' => ['user_id']]
                );
                $routes->fallbacks('InflectedRoute');
            }
        );

        Router::scope(
            '/comments',
            function (RouteBuilder $routes) {
                Router::connect(
                    '/comments/add',
                    ['controller' => 'PostComments', 'action' => 'addComment']
                );
                Router::connect(
                    '/comments/view/:post_id',
                    ['controller' => 'PostComments', 'action' => 'viewComment'],
                    ['pass' => ['post_id']]
                );
                $routes->fallbacks('InflectedRoute');
            }
        );

        Router::scope(
            '/likes',
            function (RouteBuilder $routes) {
                Router::connect(
                    '/posts/like',
                    ['controller' => 'PostLikes', 'action' => 'likePost']
                );
                Router::connect(
                    '/posts/unlike',
                    ['controller' => 'PostLikes', 'action' => 'unlikePost']
                );
                Router::connect(
                    '/posts/total-likes/:post_id',
                    ['controller' => 'PostLikes', 'action' => 'totalLike'],
                    ['pass' => ['post_id']]
                );
                $routes->fallbacks('InflectedRoute');
            }
        );

        Router::scope(
            '/error',
            function (RouteBuilder $routes) {
                Router::connect(
                    '/not-found',
                    ['controller' => 'Error', 'action' => 'notFound']
                );
                $routes->fallbacks('InflectedRoute');
            }
        );

        $routes->fallbacks(DashedRoute::class);
    }
);
