<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WhitelistedTokenTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WhitelistedTokenTable Test Case
 */
class WhitelistedTokenTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WhitelistedTokenTable
     */
    public $WhitelistedToken;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WhitelistedToken'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('WhitelistedToken') ? [] : ['className' => WhitelistedTokenTable::class];
        $this->WhitelistedToken = TableRegistry::getTableLocator()->get('WhitelistedToken', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WhitelistedToken);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
