<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    /**
     * This is for post creation
     *
     * @return $this->CommonResponses
     */
    public function addPost()
    {
        if ($this->request->is('post')) {
            $largeFile = $this->Posts->phpContentWarning();
            if ($largeFile != false) {
                return $this->CommonResponses->validationError($largeFile);
            }
            $user_id = $this->Token->userId();
            $data = $this->request->data();
            $posts = $this->Posts->newEntity();
            $posts = $this->Posts->patchEntity($posts, $data);
            $error = $posts->getErrors();
            if (!$this->Validator->validate($data, 'PostAddForm', $error)) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $image = $this->Posts->identifyImage($this->request->data('image'), 'blog');
            $posts->image = $image;
            $posts->user_id = $user_id;
            if ($this->Posts->save($posts)) {
                $message = __('Post successfully created');

                return $this->CommonResponses->success($message, null);
            }

            return $this->CommonResponses->NotFound();
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This is for updating a Post
     *
     * @param [type] $post_id Post id
     * @return $this->CommonResponses
     */
    public function editPost($post_id = null)
    {
        if ($this->request->is('post')) {
            $largeFile = $this->Posts->phpContentWarning();
            if ($largeFile != false) {
                return $this->CommonResponses->validationError($largeFile);
            }
            $user_id = $this->Token->userId();
            $data = ['user_id' => $user_id, 'post_id' => $post_id];
            $params = $this->request->data();
            $data = array_merge($params, $data);
            if (!$this->Validator->validate($data, 'PostEditForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $posts = $this->Posts->find('PostByOwner', $data);
            if (empty($posts)) {
                $message_id = 'POST_NOT_FOUND';
                $message = __('Post not found in the table');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $posts = $this->Posts->patchEntity($posts, $this->request->data);
            $image = $this->Posts->identifyImage($this->request->data('image'), 'blog');
            (!$image != "") ?: $posts->image = $image;
            if ($posts->isDirty()) {
                if ($this->Posts->save($posts)) {
                    $message = 'Post successfully updated';

                    return $this->CommonResponses->success($message, null);
                }
            }
            $message_id = 'NOTHING_CHANGE';
            $message = __('You inputted data is identical in your current record');

            return $this->CommonResponses->logicalError($message_id, $message);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Viewing a post
     *
     * @return $this->CommonResponses
     */
    public function viewPost()
    {
        if ($this->request->is('get')) {
            $data = $this->request->getQueryParams();
            if (!$this->Validator->validate($data, 'PaginationForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $posts = $this->paginate(
                $this->Posts
                    ->find(
                        'NewsFeedPost',
                        ['search' => $this->request->query('search')]
                    )
            );

            if ($posts == 'PAGE_NOT_FOUND') {
                return $this->CommonResponses->pageNotFound();
            }
            if (count($posts) == 0) {
                $message_id = 'POST_NOT_FOUND';
                $message = __('Post not found in the table');

                return $this->CommonResponses->logicalError($message_id, $message);
            }

            $message = __('Successfully view all posts in your news feed');
            $data = $posts;

            return $this->CommonResponses->success($message, $data);
        } else {
            return $this->CommonResponses->methodNotAllowed();
        }
    }

    /**
     * This for viewing a specific post
     *
     * @param [type] $user_id User id
     * @return $this->CommonResponses
     */
    public function viewUserPost($user_id = null)
    {
        if ($this->request->is('get')) {
            $params = $this->request->getQueryParams();
            $data = array_merge($params, ['user_id' => $user_id, 'search' => $this->request->query('search')]);
            $error = [];
            if (!$this->Validator->validate($data, 'UserIdForm', [])) {
                $error = $this->Validator->errors();
            }
            if (!$this->Validator->validate($data, 'PaginationForm', $error)) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $posts = $this->paginate(
                $this->Posts
                    ->find('UserPost', $data)
            );

            $data = $posts;
            if ($posts == 'PAGE_NOT_FOUND') {
                return $this->CommonResponses->pageNotFound();
            }
            if (count($posts) == 0) {
                $message_id = 'EMPTY_POST';
                $message = __('No post current recorded');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $message = __('Successfully view all posts of this user');

            return $this->CommonResponses->success($message, $data);
        } else {
            return $this->CommonResponses->methodNotAllowed();
        }
    }

    /**
     * Viewing specific post only
     *
     * @param [type] $post_id post id
     * @return $this->CommonResponses
     */
    public function viewPostInfo($post_id = null)
    {
        if ($this->request->is('get')) {
            $data = ['post_id' => $post_id];
            if (!$this->Validator->validate($data, 'PostIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->post($post_id)) !== true) {
                return $res;
            }
            $posts = $this->Posts
                ->find('PostFields')
                ->find('PostContainedByUser', $data);
            $message = __('Successfully view the posts information');

            return $this->CommonResponses->success($message, $posts);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This is for removing the post
     *
     * @return $this->CommonResponses
     */
    public function removePost()
    {
        if ($this->request->is('delete')) {
            $user_id = $this->Token->userId();
            $params = $this->request->data();
            $data = array_merge($params, ['user_id' => $user_id]);
            if (!$this->Validator->validate($data, 'PostIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $post = $this->Posts->find('PostByOwner', $data);
            if ($post == null) {
                $message_id = 'POST_NOT_FOUND';
                $message = __('Post not found in the table');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $post = $this->Posts->patchEntity($post, $data);
            $post->is_deleted = 1;
            if ($this->Posts->save($post)) {
                $message = __('Successfully deleted the post');

                return $this->CommonResponses->success($message, null);
            }
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This is for sharing a post
     *
     * @return $this->CommonResponses
     */
    public function sharePost()
    {
        if ($this->request->is('post')) {
            $user_id = $this->Token->userId();
            $data = $this->request->data();
            $data = array_merge($data, ['user_id' => $user_id]);
            if (!$this->Validator->validate($data, 'SharePostForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $posts = $this->Posts->find('PostInfo', ['post_id' => $data['post_id']]);
            if ($posts == null) {
                $message_id = 'POST_NOT_FOUND';
                $message = __('Post not found in the table');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $parentPostId = $this->Posts->getParentPost($posts);
            $posts = $this->Posts->newEntity();
            $posts = $this->Posts->patchEntity($posts, $data);
            $posts->parent_post_id = $parentPostId;
            if ($this->Posts->save($posts)) {
                $message = __('Successfully shared the post');

                return $this->CommonResponses->success($message, null);
            }
        }

        return $this->CommonResponses->methodNotAllowed();
    }
}
