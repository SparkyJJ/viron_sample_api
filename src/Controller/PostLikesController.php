<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PostLikes Controller
 *
 * @property \App\Model\Table\PostLikesTable $PostLikes
 *
 * @method \App\Model\Entity\PostLike[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostLikesController extends AppController
{
    /**
     * Like the post
     *
     * @return $this->CommonResponses
     */
    public function likePost()
    {
        if ($this->request->is('post')) {
            $userId = $this->Token->userId();
            $postId = $this->request->data('post_id');
            $postUser = $postId . "_" . $userId;
            $data = ['post_user' => $postUser, 'user_id' => $userId];
            $params = $this->request->data();
            $data = array_merge($params, $data);
            if (!$this->Validator->validate($data, 'PostIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->post($postId)) !== true) {
                return $res;
            }
            $likeStatus = $this->PostLikes->checkLike($data);
            if ($likeStatus == 'NOT_EXISTED') {
                $like = $this->PostLikes->newEntity();
                $like = $this->PostLikes->patchEntity($like, $data);
                $this->PostLikes->save($like);
            } elseif ($likeStatus == 'LIKED') {
                $message_id = 'ALREADY_LIKE';
                $message = __('Already like this post');

                return $this->CommonResponses->logicalError($message_id, $message);
            } elseif ($likeStatus == 'UNLIKED') {
                $like = $this->PostLikes->findByPostUser($postUser)->first();
                $like = $this->PostLikes->patchEntity($like, $data);
                $like->is_deleted = 0;
                $this->PostLikes->save($like);
            }
            $message = __('Successfully liked the post');

            return $this->CommonResponses->success($message, null);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Unlike the post
     *
     * @return $this->CommonResponses
     */
    public function unlikePost()
    {
        if ($this->request->is('post')) {
            $userId = $this->Token->userId();
            $postId = $this->request->data('post_id');
            $postUser = $postId . "_" . $userId;
            $data = ['post_user' => $postUser, 'user_id' => $userId];
            $params = $this->request->data();
            $data = array_merge($params, $data);
            if (!$this->Validator->validate($data, 'PostIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->post($postId)) !== true) {
                return $res;
            }
            $likeStatus = $this->PostLikes->checkLike($data);
            if ($likeStatus == 'NOT_EXISTED' || $likeStatus == 'UNLIKED') {
                $message_id = 'ALREADY_UNLIKE';
                $message = __('Already unliked this post');

                return $this->CommonResponses->logicalError($message_id, $message);
            } elseif ($likeStatus == 'LIKED') {
                $like = $this->PostLikes->findByPostUser($postUser)->first();
                $like = $this->PostLikes->patchEntity($like, $data);
                $like->is_deleted = 1;
                $this->PostLikes->save($like);
                $message = __('Successfully unlike the post');

                return $this->CommonResponses->success($message, null);
            }
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Get the total like of the post
     *
     * @param [type] $post_id post id
     * @return $this->CommonResponses
     */
    public function totalLike($post_id = null)
    {
        if ($this->request->is('get')) {
            if (!$this->Validator->validate(['post_id' => $post_id], 'PostIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->post($post_id)) !== true) {
                return $res;
            }
            $total = $this->PostLikes->findByPostId($post_id)->count();
            $data = ['total' => $total];
            $message = __('Successfully get the total like of the post');

            return $this->CommonResponses->success($message, $data);
        }

        return $this->CommonResponses->methodNotAllowed();
    }
}
