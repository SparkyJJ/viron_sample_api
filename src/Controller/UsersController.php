<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Http\Cookie\Cookie;
use Cake\Http\ServerRequest;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Psy\CodeCleaner\NoReturnValue;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Intiatlize auth allow
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'register', 'logout', 'activateUserAccount', 'NotFound']);
    }

    /**
     * Before filter
     *
     * @param Event $event object
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * Login User to generate token
     *
     * @return $this->CommonResponses
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['activated'] == 0) {
                    $message_id = 'ACCOUNT_NOT_ACTIVATED';
                    $message = __('Please activated your account via email');

                    return $this->CommonResponses->logicalError($message_id, $message);
                }
                $message = __('User successfully login');
                $token = $this->Token->generate($user['id']);
                $data = ['token' => $token];

                return $this->CommonResponses->success($message, $data);
            }
            $message_id = 'INVALID_CREDENTIALS';
            $message = __('Invalid Username or password');

            return $this->CommonResponses->logicalError($message_id, $message);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Register
     *
     * @return $this->CommonResponses
     */
    public function register()
    {
        if ($this->request->is('post')) {
            $largeFile = $this->Users->phpContentWarning();
            if ($largeFile != false) {
                return $this->CommonResponses->validationError($largeFile);
            }
            $data = $this->request->getData();
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $data);
            $error = $user->getErrors();
            if (!$this->Validator->validate($data, 'RegisterForm', $error)) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $image = $this->Users->identifyImage($this->request->data('image'), 'profile');
            $user->image = $image;
            $user->activation_code = uniqid();
            if ($this->Users->save($user)) {
                $fullname = $user->firstname . " " . $user->lastname;
                $this->mail($user->email, $user->activation_code, $fullname);
                $message = __('User successfully register and pleased activate your account in your email');

                return $this->CommonResponses->success($message, null);
            }

            return $this->CommonResponses->NotFound();
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This is used to send activation link via email
     *
     * @param [string] $emailAddress -> email address of receiver
     * @param [string] $activationCode -> uniqid
     * @param [string] $fullname -> User name
     * @return void
     */
    public function mail($emailAddress, $activationCode, $fullname)
    {
        try {
            $host = $this->request->host();
            $message = [
                'link' => "http://" . $host . "/users/activate/" . $emailAddress . "/" . $activationCode,
                'fullname' => $fullname
            ];
            $email = new Email('default');
            $email->transport('mail');
            $email
                ->template('activation', null)
                ->emailFormat('html')
                ->from(['MicroBlog.chaste@email.com.ph' => 'MicroBlog Chaste'])
                ->to($emailAddress)
                ->cc('macayananjoselin@gmail.com')
                ->subject('Activate account in MicroBlog Chaste')
                ->send(json_encode($message));
        } catch (\Throwable $th) {
            die('Email not sent');
        }
    }

    /**
     * This will activate the user account
     *
     * @param [type] $email -> email address to be activated
     * @param [type] $activateCode -> uniqId
     * @return $this->CommonResponses
     */
    public function activateUserAccount($email = null, $activateCode = null)
    {
        if ($this->request->is('get')) {
            $user = $this->Users
                ->findByEmail($email)
                ->where(
                    [
                        'activation_code' => $activateCode,
                        'activated' => 0
                    ]
                )
                ->first();
            if ($user != null) {
                $user->activated = 1;
                $user->activation_code = uniqid();
                if ($this->Users->save($user)) {
                    die('ACTIVATED SUCCESS [This will redirect in the App login page]');
                }
            } else {
                die('ACTIVATION FAILED [This will redirect in the App login page]');
            }
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This function update user information
     *
     * @return $this->CommonResponses
     */
    public function editUser()
    {
        if ($this->request->is('post')) {
            $largeFile = $this->Users->phpContentWarning();
            if ($largeFile != false) {
                return $this->CommonResponses->validationError($largeFile);
            }
            $user_id = $this->Token->userId();
            $user = $this->Users->get($user_id);
            $data = $this->request->data();
            $user = $this->Users->patchEntity($user, $data);
            $error = $user->getErrors();
            if (!$this->Validator->validate($data, 'UserEditForm', $error)) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $image = $this->Users->identifyImage($this->request->data('image'), 'profile');
            (!$image != "") ?: $user->image = $image;
            if ($user->isDirty()) {
                if ($this->Users->save($user)) {
                    $message = __('User successfully updated');

                    return $this->CommonResponses->success($message, null);
                }
            }
            $message_id = 'NOTHING_CHANGE';
            $message = __('You inputted data is identical in your current record');

            return $this->CommonResponses->logicalError($message_id, $message);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * This function update user information
     *
     * @return $this->CommonResponses
     */
    public function changePassword()
    {
        if ($this->request->is('put')) {
            $user_id = $this->Token->userId();
            $user = $this->Users->get($user_id);
            $params = $this->request->data();
            $data = array_merge($params, ['id' => $user_id]);
            $user = $this->Users->patchEntity($user, $data);
            if (!$this->Validator->validate($data, 'PasswordChangeForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if ($this->Users->save($user)) {
                $message = __('User successfully change the password');

                return $this->CommonResponses->success($message, null);
            }
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * Viewing specific user
     *
     * @param [type] $user_id User id for a post
     * @return $this->CommonResponses
     */
    public function viewUserInfo($user_id = null)
    {
        if ($this->request->is('get')) {
            $data = ['user_id' => $user_id];
            if (!$this->Validator->validate($data, 'UserIdForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            if (($res = $this->CheckTableId->user($user_id)) !== true) {
                return $res;
            }
            $user = $this->Users
                ->findById($user_id)
                ->find('UserFields')
                ->first();
            $message = __('Successfully view the user');

            return $this->CommonResponses->success($message, $user);
        }

        return $this->CommonResponses->methodNotAllowed();
    }

    /**
     * View all user
     *
     * @return $this->CommonResponses
     */
    public function viewUser()
    {
        if ($this->request->is('get')) {
            $data = $this->request->getQueryParams();
            if (!$this->Validator->validate($data, 'PaginationForm', [])) {
                $error = $this->Validator->errors();

                return $this->CommonResponses->validationError($error);
            }
            $users = $this->paginate(
                $this->Users
                    ->find(
                        'finder',
                        ['name' => $this->request->query('search')]
                    )
                    ->find('UserFields')
                    ->where(['Users.activated' => 1])
            );
            if ($users == 'PAGE_NOT_FOUND') {
                return $this->CommonResponses->pageNotFound();
            }
            if (count($users) == 0) {
                $message_id = 'USER_NOT_FOUND';
                $message = __('User not found in the table');

                return $this->CommonResponses->logicalError($message_id, $message);
            }
            $message = __('Successfully view all users');
            $data = $users;

            return $this->CommonResponses->success($message, $data);
        } else {
            return $this->CommonResponses->methodNotAllowed();
        }
    }

    /**
     * Invalidate the token
     *
     * @return $this->CommonResponses
     */
    public function logout()
    {
        if ($this->Token->destroy()) {
            $message = __('Logout successfully');

            return $this->CommonResponses->success($message, null);
        } else {
            $message = __('Already logout');

            return $this->CommonResponses->success($message, null);
        }
    }
}
