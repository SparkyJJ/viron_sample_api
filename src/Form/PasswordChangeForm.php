<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use App\Model\Validation\PasswordValidator;
use App\Model\Validation\ConfirmPasswordValidator;
use App\Model\Validation\CurrentPasswordValidator;

/**
 * Validator for changing a password
 */
class PasswordChangeForm extends Form
{
    /**
     * Schema
     *
     * @param Schema $schema
     * @return $schema
     */
    public function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Validator
     *
     * @param Validator $validator
     * @return $validator
     */
    public function _buildValidator(Validator $validator)
    {
        $passwordValidator = new PasswordValidator();
        $validator = $passwordValidator->validationDefault($validator);
        $validator
            ->requirePresence('password', 'create')
            ->requirePresence('id', 'create')
            ->notEmptyString('password');

        $confirmPasswordValidator = new ConfirmPasswordValidator();
        $validator = $confirmPasswordValidator->validationDefault($validator);
        $validator
            ->requirePresence('confirm_password', 'create')
            ->notEmptyString('confirm_password');

        $currentPasswordValidator = new CurrentPasswordValidator();
        $validator = $currentPasswordValidator->validationDefault($validator);
        $validator
            ->requirePresence('current_password', 'create')
            ->notEmptyString('current_password');

        return $validator;
    }

    /**
     * Execute
     *
     * @param array $data array
     * @return true
     */
    public function _execute(array $data)
    {
        // Send an email.
        return true;
    }

    /**
     * Set Errors
     *
     * @param [type] $errors error
     * @return void
     */
    public function _setErrors($errors)
    {
        $this->_errors = $errors;
    }
}
