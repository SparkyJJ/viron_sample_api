<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use App\Model\Validation\UsernameValidator;
use App\Model\Validation\PasswordValidator;
use App\Model\Validation\ConfirmPasswordValidator;
use App\Model\Validation\FirstnameValidator;
use App\Model\Validation\LastnameValidator;
use App\Model\Validation\LocalNameValidator;
use App\Model\Validation\BirthDayValidator;
use App\Model\Validation\LotBlockValidator;
use App\Model\Validation\StreetValidator;
use App\Model\Validation\CountryValidator;
use App\Model\Validation\ZipCodeValidator;
use App\Model\Validation\EmailValidator;
use App\Model\Validation\PhoneNoValidator;
use App\Model\Validation\ImageValidator;
use App\Model\Validation\BioValidator;

/**
 * Validator for registering a user
 */
class RegisterForm extends Form
{
    /**
     * Schema
     *
     * @param Schema $schema
     * @return $schema
     */
    public function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Validator
     *
     * @param Validator $validator
     * @return $validator
     */
    public function _buildValidator(Validator $validator)
    {
        $usernameValidation = new UsernameValidator();
        $validator = $usernameValidation->validationDefault($validator);

        $passwordValidator = new PasswordValidator();
        $validator = $passwordValidator->validationDefault($validator);
        $validator
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $confirmPasswordValidator = new ConfirmPasswordValidator();
        $validator = $confirmPasswordValidator->validationDefault($validator);
        $validator
            ->requirePresence('confirm_password', 'create')
            ->notEmptyString('confirm_password');

        $firstnameValidator = new FirstnameValidator();
        $validator = $firstnameValidator->validationDefault($validator);

        $lastnameValidator = new LastnameValidator();
        $validator = $lastnameValidator->validationDefault($validator);

        $localNameValidator = new LocalNameValidator();
        $validator = $localNameValidator->validationDefault($validator);

        $birthDayValidator = new BirthDayValidator();
        $validator = $birthDayValidator->validationDefault($validator);

        $lotBlockValidator = new LotBlockValidator();
        $validator = $lotBlockValidator->validationDefault($validator);

        $streetValidator = new streetValidator();
        $validator = $streetValidator->validationDefault($validator);

        $countryValidator = new CountryValidator();
        $validator = $countryValidator->validationDefault($validator);

        $zipCodeValidator = new ZipCodeValidator();
        $validator = $zipCodeValidator->validationDefault($validator);

        $emailValidator = new EmailValidator();
        $validator = $emailValidator->validationDefault($validator);

        $phoneNoValidator = new PhoneNoValidator();
        $validator = $phoneNoValidator->validationDefault($validator);

        $imageValidator = new ImageValidator();
        $validator = $imageValidator->validationDefault($validator);

        $bioValidator = new BioValidator();
        $validator = $bioValidator->validationDefault($validator);

        return $validator;
    }

    /**
     * Execute
     *
     * @param array $data array
     * @return true
     */
    public function _execute(array $data)
    {
        // Send an email.
        return true;
    }

    /**
     * Set Errors
     *
     * @param [type] $errors error
     * @return void
     */
    public function _setErrors($errors)
    {
        $this->_errors = $errors;
    }
}
