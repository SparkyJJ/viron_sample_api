<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use App\Model\Validation\UserIdValidator;

/**
 * Validator for User ID
 */
class UserIdForm extends Form
{
    /**
     * Schema
     *
     * @param Schema $schema
     * @return $schema
     */
    public function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Validator
     *
     * @param Validator $validator
     * @return $validator
     */
    public function _buildValidator(Validator $validator)
    {
        $userIdValidator = new UserIdValidator();
        $validator = $userIdValidator->validationDefault($validator);

        return $validator;
    }

    /**
     * Execute
     *
     * @param array $data array
     * @return true
     */
    public function _execute(array $data)
    {
        // Send an email.
        return true;
    }

    /**
     * Set Errors
     *
     * @param [type] $errors error
     * @return void
     */
    public function _setErrors($errors)
    {
        $this->_errors = $errors;
    }
}
