<?php

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use App\Model\Validation\SearchValidator;
use App\Model\Validation\LimitValidator;
use App\Model\Validation\PageValidator;

/**
 * Validator for Post pagination
 */
class PaginationForm extends Form
{
    /**
     * Schema
     *
     * @param Schema $schema
     * @return $schema
     */
    public function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Validator
     *
     * @param Validator $validator
     * @return $validator
     */
    public function _buildValidator(Validator $validator)
    {
        $searchValidator = new SearchValidator();
        $validator = $searchValidator->validationDefault($validator);

        $limitValidator = new LimitValidator();
        $validator = $limitValidator->validationDefault($validator);

        $pageValidator = new PageValidator();
        $validator = $pageValidator->validationDefault($validator);

        return $validator;
    }

    /**
     * Execute
     *
     * @param array $data array
     * @return true
     */
    public function _execute(array $data)
    {
        // Send an email.
        return true;
    }

    /**
     * Set Errors
     *
     * @param [type] $errors error
     * @return void
     */
    public function _setErrors($errors)
    {
        $this->_errors = $errors;
    }
}
