<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

define("NOT_EXISTED", null);
define("UNLIKED", null);

class LikeBehavior extends Behavior
{
    /**
     * Check if the the like already added in the db
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findCheckLikeExist(Query $query, array $options)
    {
        return $query
            ->where(['post_user' => $options['post_user']])
            ->first();
    }

    /**
     * Check if already like the post
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findCheckLikeStatus(Query $query, array $options)
    {
        return $query
            ->where(['post_user' => $options['post_user']])
            ->where(['is_deleted' => 0])
            ->first();
    }

    /**
     * Return to the controller the like status
     *
     * @param [type] $data object
     * @return 'NOT_EXISTED'||'LIKE'
     */
    public function checkLike($data)
    {
        $like = TableRegistry::get('PostLikes')->find('CheckLikeExist', $data);
        if ($like == NOT_EXISTED) {
            return 'NOT_EXISTED';
        } else {
            $like = TableRegistry::get('PostLikes')->find('CheckLikeStatus', $data);
            if ($like == UNLIKED) {
                return 'UNLIKED';
            } else {
                return 'LIKED';
            }
        }
    }
}
