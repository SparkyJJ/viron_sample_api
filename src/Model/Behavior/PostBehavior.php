<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;

class PostBehavior extends Behavior
{
    /**
     * This is for getting a parent post id
     *
     * @param [type] $post object
     * @return $post->id
     */
    public function getParentPost($post)
    {
        if ($post->parent_post_id != null) {
            return $post->parent_post_id;
        }

        return $post->id;
    }

    /**
     * This is for getting PostI information
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findPostInfo(Query $query, array $options)
    {
        return $query
            ->where(['id' => $options['post_id']])
            ->where(['is_deleted' => 0])
            ->first();
    }

    /**
     * Finding out if the user owned the post
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findPostByOwner(Query $query, array $options)
    {
        return $query
            ->where(['id' => $options['post_id']])
            ->where(['user_id' => $options['user_id']])
            ->where(['is_deleted' => 0])
            ->first();
    }

    /**
     * Finding a post with USER contained
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findPostContainedByUser(Query $query, array $options)
    {
        return $query
            ->where(['Posts.id' => $options['post_id']])
            ->where(['Posts.is_deleted' => 0])
            ->contain(['Users'])
            ->first();
    }

    /**
     * This for NewsFeed
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findNewsFeedPost(Query $query, array $options)
    {
        return $query
            ->where(
                [
                    'OR' => [
                        'lower(Posts.description) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(Users.firstname) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(Users.lastname) LIKE' => strtolower("%" . $options['search'] . "%")
                    ]
                ]
            )
            ->where(['Posts.is_deleted !=' => 1])
            ->orderDesc('Posts.id')
            ->contain(
                [
                    'Users' => [
                        'fields' => [
                            'id',
                            'firstname',
                            'lastname',
                            'image'
                        ]
                    ]
                ]
            )
            ->contain(
                [
                    'ParentPosts' => [
                        'Users' => [
                            'fields' => [
                                'id',
                                'firstname',
                                'lastname',
                                'image'
                            ]
                        ]
                    ]
                ]
            )
            ->contain(['hasRelation']);
    }

    public function findTestParentUser(Query $query)
    {
        return $query
            ->where([(int) 'ParentUsers.id' => (int) 'ParentPosts.user_id']);
    }
    /**
     * Return query in the controller for user post
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findUserPost(Query $query, array $options)
    {
        return $query
            ->where(
                [
                    'OR' => [
                        'lower(Posts.description) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(Users.firstname) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(Users.lastname) LIKE' => strtolower("%" . $options['search'] . "%")
                    ]
                ]
            )
            ->where(['Posts.user_id' => $options['user_id']])
            ->where(['Posts.is_deleted !=' => 1])
            ->orderDesc('Posts.id')
            ->contain(
                [
                    'Users' => [
                        'fields' => [
                            'id',
                            'firstname',
                            'lastname',
                            'image'
                        ]
                    ]
                ]
            )
            ->contain(
                [
                    'ParentPosts' => [
                        'Users' => [
                            'fields' => [
                                'id',
                                'firstname',
                                'lastname',
                                'image'
                            ]
                        ]
                    ]
                ]
            )
            ->contain(['hasRelation']);
    }
}
