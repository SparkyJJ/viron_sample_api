<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;

/**
 * Custom select in the table
 */
class CustomSelectBehavior extends Behavior
{
    /**
     * This is a custom select in user fields
     *
     * @param Query $query query
     * @return $query query
     */
    public function findUserFields(Query $query)
    {
        return $query
            ->select(
                [
                    'Users.id',
                    'Users.firstname',
                    'Users.lastname',
                    'Users.birthday',
                    'Users.lot_block',
                    'Users.street',
                    'Users.local_name',
                    'Users.country',
                    'Users.image',
                    'Users.phone_no',
                    'Users.bio'
                ]
            );
    }

    /**
     * Custom select post fields
     *
     * @param Query $query query
     * @return $query
     */
    public function findPostFields(Query $query)
    {
        return $query
            ->select(
                [
                    'Posts.id',
                    'Posts.description',
                    'Posts.image',
                    'Posts.created'
                ]
            );
    }

    /**
     * Custom select for following fields
     *
     * @param Query $query query
     * @return $query
     */
    public function findFollowingFields(Query $query)
    {
        return $query
            ->select(
                [
                    'Followers.id',
                    'Followers.modified',
                    'userFollowing.id',
                    'userFollowing.firstname',
                    'userFollowing.lastname',
                    'userFollowing.birthday',
                    'userFollowing.lot_block',
                    'userFollowing.street',
                    'userFollowing.local_name',
                    'userFollowing.country',
                    'userFollowing.image',
                    'userFollowing.phone_no',
                    'userFollowing.bio'
                ]
            );
    }

    /**
     * Custom select for Follower fields
     *
     * @param Query $query query
     * @return $query
     */
    public function findFollowerFields(Query $query)
    {
        return $query
            ->select(
                [
                    'Followers.id',
                    'Followers.modified',
                    'userFollower.id',
                    'userFollower.firstname',
                    'userFollower.lastname',
                    'userFollower.birthday',
                    'userFollower.lot_block',
                    'userFollower.street',
                    'userFollower.local_name',
                    'userFollower.country',
                    'userFollower.image',
                    'userFollower.phone_no',
                    'userFollower.bio'
                ]
            );
    }

    public function findCommentsFields(Query $query)
    {
        return $query
            ->select(
                [
                    'PostComments.id',
                    'PostComments.description',
                    'PostComments.created',
                    'Users.id',
                    'Users.firstname',
                    'Users.lastname',
                    'Users.birthday',
                    'Users.lot_block',
                    'Users.street',
                    'Users.local_name',
                    'Users.country',
                    'Users.image',
                    'Users.phone_no',
                    'Users.bio'
                ]
            );
    }
}
