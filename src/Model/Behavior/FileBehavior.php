<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;

class FileBehavior extends Behavior
{
    /**
     * Error handler for PHP warning File upload
     *
     * @return $error
     */
    public function phpContentWarning()
    {
        $contentWarning = false;
        if (isset($_SERVER["CONTENT_LENGTH"])) {
            if ($_SERVER["CONTENT_LENGTH"] > ((int)ini_get('post_max_size') * 1024 * 1024)) {
                $contentWarning = true;
            }
        }
        ob_end_clean();
        if ($contentWarning) {
            $error = [
                'image' => [
                    'INVALID_SIZE' => __('WARNING: This file is too large to upload')
                ]
            ];

            return $error;
        }

        return false;
    }

    /**
     * This is used to upload image in Post and profile
     *
     * @param [type] $imgObj -> image object
     * @param [type] $imgNameWillBeUse -> unique name of the image
     * @param [type] $field -> table field
     * @return $return
     */
    public function uploadImage($imgObj, $imgNameWillBeUse, $field)
    {
        $return = false;
        $imgName = $imgNameWillBeUse;
        $imgTemp = $imgObj['tmp_name'];
        $imgPath = "img/" . $field . "/" . $imgName;
        if (move_uploaded_file($imgTemp, WWW_ROOT . $imgPath)) {
            $return = true;
        } else {
            $return = false;
        }
        /** Return boolean status */
        return $return;
    }

    /**
     * Set a unique filename for image
     *
     * @param [type] $imgName -> unique image name
     * @param [type] $filename -> image name
     * @return $data
     */
    public function getUniqueFileName($imgName, $filename)
    {
        $tmp = explode(".", $filename);
        $extension = end($tmp);
        $data = $imgName . '.' . $extension;
        /** Return the file name with extension type */
        return $data;
    }

    /**
     * Identify image if need to be upload
     *
     * @param [type] $imageData image object
     * @param [type] $field location of the folder
     * @return $imgName
     */
    public function identifyImage($imageData, $field)
    {
        $imgName = '';
        if ($imageData != null) {
            $imageData['name'] = $this->getUniqueFileName(uniqid(), $imageData['name']);
            $this->uploadImage($imageData, $imageData['name'], $field);
            $imgName = $imageData['name'];
        }

        return $imgName;
    }
}
