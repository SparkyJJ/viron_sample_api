<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

define("NOT_EXISTED", null);
define("UNFOLLOW", null);
define("NO_RELATION", 0);

class FollowerBehavior extends Behavior
{
    /**
     * Check if already added in the sv
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findCheckFollowExist(Query $query, array $options)
    {
        return $query
            ->where(['from_to' => $options['from_to']])
            ->first();
    }

    /**
     * Check if the user already followed or not
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findCheckFollowStatus(Query $query, array $options)
    {
        return $query
            ->where(['from_to' => $options['from_to']])
            ->where(['is_deleted' => 0])
            ->first();
    }

    /**
     * Return to the controller the user status
     *
     * @param [type] $data object
     * @return string
     */
    public function checkUserFollow($data)
    {
        $follow = TableRegistry::get('Followers')->find('CheckFollowExist', $data);
        if ($follow == NOT_EXISTED) {
            return 'NOT_EXISTED';
        } else {
            $follow = TableRegistry::get('Followers')->find('CheckFollowStatus', $data);
            if ($follow == UNFOLLOW) {
                return 'UNFOLLOW';
            } else {
                return 'FOLLOW';
            }
        }
    }

    /**
     * Check the user if it has a following relation
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findUserFollowingRelation(Query $query, array $options)
    {
        return $query
            ->where(
                [
                    'OR' => [
                        'lower(userFollowing.firstname) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(userFollowing.lastname) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(userFollowing.email) LIKE' => strtolower("%" . $options['search'] . "%")
                    ]
                ]
            )
            ->where(['Followers.user_id_from' => intval($options['user_id'])])
            ->contain('userFollowing');
    }

    /**
     * Check the user if it has a follower relation
     *
     * @param Query $query query
     * @param array $options array
     * @return $query
     */
    public function findUserFollowerRelation(Query $query, array $options)
    {
        return $query
            ->where(
                [
                    'OR' => [
                        'lower(userFollower.firstname) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(userFollower.lastname) LIKE' => strtolower("%" . $options['search'] . "%"),
                        'lower(userFollower.email) LIKE' => strtolower("%" . $options['search'] . "%")
                    ]
                ]
            )
            ->where(['Followers.user_id_to' => intval($options['user_id'])])
            ->contain('userFollower');
    }

    /**
     * Check the user if it has a relation
     *
     * @param [type] $data array
     * @return true||false
     */
    public function checkStatus($data)
    {
        $user = TableRegistry::get('Followers')
            ->find()
            ->where(['Followers.user_id_to' => intval($data['user_id_to'])])
            ->where(['Followers.user_id_from' => $data['user_id_from']])
            ->count();
        if ($user != NO_RELATION) {
            return true;
        } else {
            return false;
        }
    }
}
