<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\PostCommentsTable&\Cake\ORM\Association\HasMany $PostComments
 * @property \App\Model\Table\PostLikesTable&\Cake\ORM\Association\HasMany $PostLikes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * This is a custom finder for User information
     *
     * @param Query $query object
     * @param array $options fields
     * @return $query
     */
    public function findFinder(Query $query, array $options)
    {
        $query->where(
            [
                'OR' => [
                    'lower(Users.username) LIKE' => strtolower("%" . $options['name'] . "%"),
                    'lower(Users.firstname) LIKE' => strtolower("%" . $options['name'] . "%"),
                    'lower(Users.lastname) LIKE' => strtolower("%" . $options['name'] . "%"),
                    'lower(Users.birthday) LIKE' => strtolower("%" . $options['name'] . "%"),
                    'lower(Users.lot_block) LIKE' => strtolower("%" . $options['name'] . "%"),
                    'lower(Users.street) LIKE' => strtolower("%" . $options['name'] . "%"),
                    'lower(Users.local_name) LIKE' => strtolower("%" . $options['name'] . "%")
                ]
            ]
        );
        /** Return query in the action */
        return $query;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('File');
        $this->addBehavior('CustomSelect');

        $this->hasMany(
            'PostComments',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->hasMany(
            'PostLikes',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->hasMany(
            'Posts',
            [
                'foreignKey' => 'user_id'
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');
        $validator->add(
            'email',
            'EMAIL_MUST_BE_UNIQUE',
            [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __('Email already exists')
            ]
        );
        $validator->add(
            'username',
            'USERNAME_MUST_BE_UNIQUE',
            [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __('Username already exists')
            ]
        );
        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
