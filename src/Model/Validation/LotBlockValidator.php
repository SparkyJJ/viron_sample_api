<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class LotBlockValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('lot_block')
            ->maxLength('lot_block', 45)
            ->requirePresence('lot_block', 'create')
            ->notEmptyString('lot_block');
        return $validator;
    }
}
