<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class BirthDayValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->date('birthday')
            ->requirePresence('birthday', 'create')
            ->notEmptyDate('birthday')
            ->add(
                'birthday',
                'INVALID_BIRTHDAY',
                [
                    'rule' => function ($value, $context) {
                        $date = $context['data']['birthday'];
                        $minAge = strtotime("-18 YEAR");
                        $maxAge = strtotime("-91 YEAR");
                        $enteredAge = strtotime($date);
                        return ($enteredAge < $minAge && $enteredAge > $maxAge)
                            ? true : false;
                    },
                    'message' => __('Minimum of 18 Years old and Maximum of 90'),
                ]
            );
        return $validator;
    }
}
