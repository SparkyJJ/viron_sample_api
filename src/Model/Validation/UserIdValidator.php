<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class UserIdValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add(
                'user_id',
                [
                    'USER_ID_INVALID' =>
                    [
                        'rule' => ['numeric'],
                        'last' => true,
                        'message' => __('User Id is based on Integer')
                    ]
                ]
            );
        return $validator;
    }
}
