<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class DescriptionValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('description')
            ->maxLength('description', 140)
            ->requirePresence('description', 'create')
            ->notEmptyString('description')
            ->add(
                'description',
                [
                    'DESCRIPTION_REQUIRED' => [
                        'rule' => 'notBlank',
                        'message' => __('Description is required'),
                    ],
                ]
            );
        return $validator;
    }
}
