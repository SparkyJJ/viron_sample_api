<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ActivatedValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('activated')
            ->notEmptyString('activated');
        return $validator;
    }
}
