<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ParentPostIdValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add(
                'parent_post_id',
                'PARENT_POST_ID_INVALID',
                [
                    'rule' => ['numeric'],
                    'message' => __('Parent post Id is based on Integer')
                ]
            );
        return $validator;
    }
}
