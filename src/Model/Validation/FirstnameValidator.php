<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class FirstnameValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('firstname')
            ->requirePresence('firstname', 'create')
            ->add(
                'firstname',
                [
                    'FIRSTNAME_REQUIRED' => [
                        'rule' => 'notBlank',
                        'message' => __('Firstname can not be empty')
                    ],
                    'FIRTNAME_INVALID_LENGTH' => [
                        'rule' => ['lengthBetween', 3, 45],
                        'message' => __('Firstname needs to be less 45 and atleast 3 characters')
                    ],
                ]
            );
        return $validator;
    }
}
