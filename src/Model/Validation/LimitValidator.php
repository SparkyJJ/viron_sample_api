<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class LimitValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('limit')
            ->add(
                'limit',
                [
                    'LIMIT_INVALID' => [
                        'rule' => ['numeric'],
                        'message' => __('The limit field is base on integers')
                    ]
                ]
            );
        return $validator;
    }
}
