<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class PageValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('page')
            ->add(
                'page',
                'PAGE_INVALID',
                [
                    'rule' => ['numeric'],
                    'message' => __('The page field is base on integer')
                ]
            );
        return $validator;
    }
}
