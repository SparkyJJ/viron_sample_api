<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class PostIdValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add(
                'post_id',
                'POST_ID_INVALID',
                [
                    'rule' => ['numeric'],
                    'last' => true,
                    'message' => __('Post Id is based on Integer')
                ]
            );
        return $validator;
    }
}
