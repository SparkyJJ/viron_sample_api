<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ImageValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator->provider('upload', \Josegonzalez\Upload\Validation\UploadValidation::class);
        $validator->provider('upload_test1', \Josegonzalez\Upload\Validation\DefaultValidation::class);
        $validator
            ->allowEmptyFile('image')
            ->add(
                'image',
                [
                    'IMAGE_INVALID_EXT' => [
                        'rule' => ['mimeType', ['image/gif', 'image/GIF', 'image/jpeg', 'image/JPEG', 'image/png', 'image/PNG', 'image/jpg', 'image/JPG']],
                        'message' => __('File extension is not supported'),
                    ],
                    'IMAGE_INVALID_SIZE' => [
                        'rule' => ['isBelowMaxSize', 1048576],
                        'message' => __('Max file size is 1MB'),
                        'provider' => 'upload'
                    ],
                    'fileUnderFormSizeLimit' => [
                        'rule' => 'isUnderFormSizeLimit',
                        'message' => __('This file is too large'),
                        'provider' => 'upload_test1'
                    ]
                ]
            );
        return $validator;
    }
}
