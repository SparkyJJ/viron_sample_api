<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class StreetValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('street')
            ->maxLength('street', 45)
            ->requirePresence('street', 'create')
            ->notEmptyString('street');
        return $validator;
    }
}
