<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;

class CurrentPasswordValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add(
                'current_password',
                'PASSWORD_NOT_MATCH',
                [
                    'rule' => function ($value, $context) {
                        $id = $context['data']['id'];
                        $user = TableRegistry::get('Users')->findById($id)->first();
                        if ($user) {
                            if ((new DefaultPasswordHasher())->check($value, $user->password)) {
                                return true;
                            }
                        }
                        /** Rutrun boolean */
                        return false;
                    },
                    'message' => __('The old password does not match the current password'),
                ]
            );
        return $validator;
    }
}
