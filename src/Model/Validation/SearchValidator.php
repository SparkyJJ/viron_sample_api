<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class SearchValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('search')
            ->add(
                'search',
                'SEARCH_INVALID_INVALID',
                [
                    'rule' => ['maxLength', 45],
                    'message' => __('Maximum length is 45 characters')
                ]
            );
        return $validator;
    }
}
