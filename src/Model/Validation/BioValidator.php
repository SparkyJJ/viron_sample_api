<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class BioValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('bio')
            ->add(
                'bio',
                [
                    'BIO_REQUIRED' => [
                        'rule' => 'notBlank',
                        'message' => __('Bio can not be empty'),
                    ],
                    'BIO_INVALID_LENGTH' => [
                        'rule' => ['maxLength', 140],
                        'message' => __('Bio needs to be less 140 characters'),
                        'last' => true
                    ]
                ]
            );
        return $validator;
    }
}
