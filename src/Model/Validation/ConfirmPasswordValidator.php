<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ConfirmPasswordValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('confirm_password')
            ->add(
                'confirm_password',
                'PASSWORD_NOT_MATCH',
                [
                    'rule' => function ($value, $context) {
                        $data = false;
                        if ($value == $context['data']['password']) {
                            $data = true;
                        }
                        /** Rutrun boolean */
                        return $data;
                    },
                    'message' => __('Password not match')
                ]
            );
        return $validator;
    }
}
