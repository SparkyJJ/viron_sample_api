<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class ActivationCodeValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('activation_code')
            ->maxLength('activation_code', 255)
            ->allowEmptyString('activation_code');
        return $validator;
    }
}
