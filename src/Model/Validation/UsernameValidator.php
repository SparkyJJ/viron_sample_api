<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class UsernameValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->add(
                'username',
                [
                    'USERNAME_INVALID_CHARACTER' => [
                        'rule' => ['custom', '/^[a-z0-9-_]*$/i'],
                        'message' => __('Alphabets, numbers, dash and underscore allowed')
                    ],
                    'USERNAME_INVALID_LENGTH' => [
                        'rule' => ['lengthBetween', 8, 20],
                        'message' => __('Username needs to be less 20 and atlest 8 characters'),
                    ],
                    'USERNAME_REQUIRED' => [
                        'rule' => 'notBlank',
                        'message' => __('Username is required'),
                    ],
                ]
            );
        return $validator;
    }
}
