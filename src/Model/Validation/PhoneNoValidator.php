<?php

namespace App\Model\Validation;

use Cake\Validation\Validator;

class PhoneNoValidator extends Validator
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('phone_no')
            ->maxLength('phone_no', 45)
            ->requirePresence('phone_no', 'create')
            ->notEmptyString('phone_no');
        return $validator;
    }
}
