<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WhitelistedToken Entity
 *
 * @property string $id
 * @property \Cake\I18n\FrozenTime|null $issue_at
 * @property \Cake\I18n\FrozenTime|null $expired_at
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 */
class WhitelistedToken extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'issue_at' => true,
        'expired_at' => true,
        'modified' => true,
        'created' => true
    ];
}
